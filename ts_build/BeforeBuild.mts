import 'source-map-support/register.js'; // This is makes error messages use the .ts file path instead. Very handy.
import fs from 'fs-extra';

export class BeforeBuild
{

    public static incrementVersion(currentVersionStr: string): string
    {
        var currentVersionArr     = currentVersionStr.split(`.`);
        currentVersionArr[2]      = String(parseInt(currentVersionArr[2]) + 1);
        var incrementedVersionNum = currentVersionArr.join(`.`);

        console.log(`   - Incrementing version to v${incrementedVersionNum}.`); // Keep for debugging.

        // Save new value to package.json.
        var packageJsonContentObj     = JSON.parse(fs.readFileSync(`package.json`, `utf8`)); // Allow combined suffix.
        packageJsonContentObj.version = incrementedVersionNum;
        fs.writeFileSync(`package.json`, JSON.stringify(packageJsonContentObj, null, 2)); // Allow combined suffix.
        return incrementedVersionNum;
    }

}
